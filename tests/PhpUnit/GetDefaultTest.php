<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MultipleClasses
declare(strict_types=1);

namespace Interitty\PhpUnit;

class GetDefaultClass
{
    /** @var string */
    protected string $foo = '';

    /**
     * Foo getter
     *
     * @return string
     */
    protected function getFoo(): string
    {
        return $this->foo;
    }
}

class GetDefaultTest extends BaseTestCase
{
    public function testSuccess(): void
    {
        $this->processTestGetDefault(GetDefaultClass::class, 'foo', '');
    }
}
