<?php

declare(strict_types=1);

namespace Interitty\PhpUnit;

use Nette\DI\MissingServiceException;

use function get_class;

class CreateContainerTest extends BaseIntegrationTestCase
{
    public function testSuccess(): void
    {
        $configContent = '
services:
    CreateContainerTest:
        class: Interitty\PhpUnit\CreateContainerTest
';
        $configFilePath = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFilePath);

        self::assertSame(CreateContainerTest::class, get_class($container->getByType(CreateContainerTest::class)));

        $thrownException = null;
        try {
            $service = $container->getByType(CreateContainerTest::class);
            self::assertSame(CreateContainerTest::class, get_class($service));
        } catch (MissingServiceException $exception) {
            $thrownException = $exception;
        }
        self::assertNull($thrownException);
    }
}
