<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MultipleClasses
declare(strict_types=1);

namespace Interitty\PhpUnit;

/** @SuppressWarnings("PHPMD.UnusedPrivateMethod") */
class CallNonPublicMethodWithArgumentClass
{
    private function processData(string $data): string
    {
        return $data;
    }
}

class CallNonPublicMethodWithArgumentTest extends BaseTestCase
{
    public function testSuccess(): void
    {
        $data = 'test';
        $class = new CallNonPublicMethodWithArgumentClass();
        $result = $this->callNonPublicMethod($class, 'processData', [$data]);
        self::assertSame($data, $result);
    }
}
