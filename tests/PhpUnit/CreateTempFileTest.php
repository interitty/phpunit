<?php

declare(strict_types=1);

namespace Interitty\PhpUnit;

use Interitty\Utils\FileSystem;

use function basename;

class CreateTempFileTest extends BaseTestCase
{
    public function testSuccess(): void
    {
        $content = 'Example of the file content';
        $fileName = 'tempFileName';
        $tempFile = $this->createTempFile($content, $fileName);
        self::assertFileExists($tempFile);
        self::assertIsWritable($tempFile);
        self::assertSame($content, FileSystem::read($tempFile));
        self::assertSame($fileName, basename($tempFile));
    }
}
