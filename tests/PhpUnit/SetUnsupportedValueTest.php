<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MultipleClasses
declare(strict_types=1);

namespace Interitty\PhpUnit;

use Interitty\Utils\Validators;

use function assert;

class SetUnsupportedValueClass
{
    /** @var string */
    protected string $foo;

    /**
     * Foo setter
     *
     * @param string $foo
     * @return static Provides fluent interface
     */
    protected function setFoo($foo)
    {
        assert(Validators::check($foo, 'string', 'Foo'));
        $this->foo = $foo;
        return $this;
    }
}

/**
 * @phpstan-import-type AssertionExceptionData from BaseTestCase
 */
class SetUnsupportedValueTest extends BaseTestCase
{
    /**
     * @phpstan-param AssertionExceptionData $data
     * @dataProvider unsupportedStringDataProvider
     */
    public function testSuccess(mixed $foo, array $data): void
    {
        $this->processTestSetUnsupportedValue(SetUnsupportedValueClass::class, 'foo', $foo, $data);
    }
}
