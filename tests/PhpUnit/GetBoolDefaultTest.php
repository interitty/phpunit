<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MultipleClasses
declare(strict_types=1);

namespace Interitty\PhpUnit;

class GetBoolDefaultClass
{
    /** @var bool */
    protected bool $foo = false;

    /**
     * Foo getter
     *
     * @return bool
     */
    protected function isFoo(): bool
    {
        return $this->foo;
    }
}

class GetBoolDefaultTest extends BaseTestCase
{
    public function testSuccess(): void
    {
        $this->processTestGetBoolDefault(GetBoolDefaultClass::class, 'foo', false);
    }
}
