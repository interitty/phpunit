<?php

declare(strict_types=1);

namespace Interitty\PhpUnit;

use Interitty\Utils\Validators;
use Nette\Utils\AssertionException;

use function sprintf;

/**
 * @coversDefaultClass Interitty\PhpUnit\DataProvider
 */
class DataProviderTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of stringDataProvider implemenattion
     *
     * @return void
     * @covers ::stringDataProvider
     * @group integration
     */
    public function testStringDataProvider(): void
    {
        foreach (self::stringDataProvider() as $name => $parameters) {
            list($data) = $parameters;

            self::assertTrue(Validators::is($data, 'string'), $name);
        }
    }

    /**
     * Tester of unsupportedStringDataProvider implemenattion
     *
     * @return void
     * @covers ::unsupportedStringDataProvider
     * @group integration
     */
    public function testUnsupportedStringDataProvider(): void
    {
        $label = 'test';
        foreach (self::unsupportedStringDataProvider() as $name => $parameters) {
            list($value, $data) = $parameters;

            self::assertFalse(Validators::is($value, 'string'), $name);
            $expectedMessage = sprintf('The %s expects to be %s, %s given', $label, $data['expected'], $data['type']);

            $excpetionMessage = '';
            try {
                Validators::assert($value, 'string', $label);
            } catch (AssertionException $exception) {
                $excpetionMessage = $exception->getMessage();
            }

            self::assertSame($expectedMessage, $excpetionMessage, $name);
        }
    }

    // </editor-fold>
}
