<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MultipleClasses
declare(strict_types=1);

namespace Interitty\PhpUnit;

use Interitty\Utils\Validators;

use function assert;

class GetUndefinedClass
{
    /** @var string */
    protected string $foo;

    /**
     * Foo getter
     *
     * @return string
     */
    protected function getFoo(): string
    {
        assert(Validators::check(isset($this->foo), 'initialized', 'foo before get'));
        return $this->foo;
    }
}

class GetUndefinedTest extends BaseTestCase
{
    public function testSuccess(): void
    {
        $this->processTestGetUndefined(GetUndefinedClass::class, 'foo');
    }
}
