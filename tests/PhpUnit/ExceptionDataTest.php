<?php

declare(strict_types=1);

namespace Interitty\PhpUnit;

use Interitty\Exceptions\Exceptions;
use LogicException;

class ExceptionDataTest extends BaseTestCase
{
    public function testExtendData(): void
    {
        $data = ['key' => 'foo'];
        $this->expectExceptionData($data);

        throw Exceptions::extend(LogicException::class)
                ->setMessage('Message with key ":key"')
                ->setData($data);
    }
}
