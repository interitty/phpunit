<?php

declare(strict_types=1);

namespace Interitty\PhpUnit;

use Generator;

class SameContentTest extends BaseTestCase
{
    public function testSuccess(): void
    {
        $content = [1, 2, 3, 4];
        $yield = static function (array $content): Generator {
            foreach ($content as $key => $value) {
                yield $key => $value;
            }
        };
        self::assertSameContent($content, $yield($content));
    }
}
