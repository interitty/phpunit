<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MultipleClasses
declare(strict_types=1);

namespace Interitty\PhpUnit;

/** @SuppressWarnings("PHPMD.UnusedPrivateField") */
class SetNonPublicPropertyClass
{
    /** @var bool */
    protected bool $propertyOne = true;

    /** @var bool */
    private bool $propertyTwo = true;
}

class SetNonPublicPropertyTest extends BaseTestCase
{
    public function testSuccess(): void
    {
        $someClass = new SetNonPublicPropertyClass();
        $this->setNonPublicPropertyValue($someClass, 'propertyOne', false);
        $this->setNonPublicPropertyValue($someClass, 'propertyTwo', false);

        self::assertFalse($this->getNonPublicPropertyValue($someClass, 'propertyOne'));
        self::assertFalse($this->getNonPublicPropertyValue($someClass, 'propertyTwo'));
    }
}
