<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MultipleClasses
declare(strict_types=1);

namespace Interitty\PhpUnit;

/** @SuppressWarnings("PHPMD.UnusedPrivateField") */
class GetNonPublicPropertyClass
{
    /** @var bool */
    protected bool $propertyOne = true;

    /** @var bool */
    private bool $propertyTwo = true;
}

class GetNonPublicPropertyTest extends BaseTestCase
{
    public function testSuccess(): void
    {
        $someClass = new GetNonPublicPropertyClass();
        $propertyOne = $this->getNonPublicPropertyValue($someClass, 'propertyOne');
        $propertyTwo = $this->getNonPublicPropertyValue($someClass, 'propertyTwo');
        self::assertSame($propertyOne, $propertyTwo);
    }
}
