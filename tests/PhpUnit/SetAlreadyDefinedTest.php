<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MultipleClasses
declare(strict_types=1);

namespace Interitty\PhpUnit;

use Interitty\Utils\Validators;

use function assert;

class SetAlreadyDefinedClass
{
    /** @var string */
    protected string $foo;

    /**
     * Foo setter
     *
     * @param string $foo
     * @return static Provides fluent interface
     */
    protected function setFoo(string $foo)
    {
        assert(Validators::check(isset($this->foo), 'uninitialized', 'foo before set'));
        $this->foo = $foo;
        return $this;
    }
}

class SetAlreadyDefinedTest extends BaseTestCase
{
    /**
     * @dataProvider stringDataProvider
     */
    public function testSuccess(string $string): void
    {
        $this->processTestSetAlreadyDefined(SetAlreadyDefinedClass::class, 'foo', $string);
    }
}
