<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MultipleClasses
declare(strict_types=1);

namespace Interitty\PhpUnit;

class GetSetClass
{
    /** @var string */
    protected string $foo = 'foo';

    /**
     * Foo getter
     *
     * @return string
     */
    protected function getFoo(): string
    {
        return $this->foo;
    }

    /**
     * Foo setter
     *
     * @param string $foo
     * @return static Provides fluent interface
     */
    protected function setFoo(string $foo)
    {
        $this->foo = $foo;
        return $this;
    }
}

class GetSetTest extends BaseTestCase
{
    /**
     * @dataProvider stringDataProvider
     */
    public function testSuccess(string $foo): void
    {
        $this->processTestGetSet(GetSetClass::class, 'foo', $foo);
    }
}
