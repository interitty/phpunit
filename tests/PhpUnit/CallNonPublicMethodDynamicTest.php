<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MultipleClasses
declare(strict_types=1);

namespace Interitty\PhpUnit;

use function function_exists;
use function PHPStan\Testing\assertType;

class CallNonPublicMethodDynamicClass
{
    /**
     * Array getter
     *
     * @return array<string>
     */
    protected function getArray(): array
    {
        return ['foo', 'bar'];
    }

    /**
     * Bool getter
     *
     * @return bool
     */
    protected function isBool(): bool
    {
        return false;
    }

    /**
     * Integer getter
     *
     * @return int
     */
    protected function getInteger(): int
    {
        return 42;
    }

    /**
     * Object getter
     *
     * @param object $object
     * @return object
     */
    protected function getObject(object $object): object
    {
        return $object;
    }
}

class CallNonPublicMethodDynamicTest extends BaseTestCase
{
    public function testSuccess(): void
    {
        $class = new CallNonPublicMethodDynamicClass();
        $array = $this->callNonPublicMethod($class, 'getArray');
        $bool = $this->callNonPublicMethod($class, 'isBool');
        $integer = $this->callNonPublicMethod($class, 'getInteger');
        $object = $this->callNonPublicMethod($class, 'getObject', [$class]);
        if (function_exists('assertType') === true) {
            // PHPStan checks
            assertType('array<string>', $array);
            assertType('bool', $bool);
            assertType('int', $integer);
            assertType('object', $object);
        }
        self::assertCount(2, $array);
    }
}
