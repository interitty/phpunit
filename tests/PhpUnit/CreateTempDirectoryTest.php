<?php

declare(strict_types=1);

namespace Interitty\PhpUnit;

use function basename;

class CreateTempDirectoryTest extends BaseTestCase
{
    public function testSuccess(): void
    {
        $directoryName = 'directoryName';
        $tempDirectory = $this->createTempDirectory($directoryName);
        self::assertFileExists($tempDirectory);
        self::assertIsWritable($tempDirectory);
        self::assertSame($directoryName, basename($tempDirectory));
    }
}
