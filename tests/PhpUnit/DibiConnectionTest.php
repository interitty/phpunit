<?php

declare(strict_types=1);

namespace Interitty\PhpUnit;

use Dibi\Connection;
use Dibi\Type;
use LogicException;

use function iterator_to_array;

class DibiConnectionTest extends BaseDibiTestCase
{
    /**
     * @inheritdoc
     */
    protected function setupDatabase(Connection $connection): void
    {
        parent::setupDatabase($connection);
        $connection->query('create table Person (id int PRIMARY KEY, name varchar NOT NULL, active bool DEFAULT 1)');
        $connection->insert('Person', ['id' => 1, 'name' => 'test 1', 'active' => true])->execute();
        $connection->insert('Person', ['id' => 2, 'name' => 'test 2', 'active' => true])->execute();
        $connection->insert('Person', ['id' => 3, 'name' => 'test 3', 'active' => false])->execute();
    }

    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of BaseDibiTestCase::createConnection exception implementation
     *
     * @return void
     * @before testDibiConnection
     */
    public function testCreateConnectionException(): void
    {
        $defaultConfig = $this->getConfig();
        try {
            $this->setConfig(['driver' => 'wrong']);
            $this->getConnection();
        } catch (LogicException $exception) {
            $message = 'Unable to create instance of Dibi driver \'Dibi\\Drivers\\WrongDriver\'.';
            self::assertSame($message, $exception->getMessage());
        }
        $this->setConfig($defaultConfig);
    }

    /**
     * Tester of dibi connection implementation
     *
     * @return void
     */
    public function testDibiConnection(): void
    {
        $expectedData = [
            ['id' => 1, 'name' => 'test 1', 'active' => true],
            ['id' => 2, 'name' => 'test 2', 'active' => true],
            ['id' => 3, 'name' => 'test 3', 'active' => false],
        ];

        $connection = $this->getConnection();
        $query = $connection->select('*')->from('Person');

        $data = $query
            ->setupResult('setRowFactory', static function (array $data): array {
                return $data;
            })
            ->setupResult('setType', 'active', Type::Bool)
            ->getIterator();

        self::assertSame($expectedData, iterator_to_array($data));
    }

    // </editor-fold>
}
