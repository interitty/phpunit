<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MultipleClasses
declare(strict_types=1);

namespace Interitty\PhpUnit;

use function function_exists;
use function PHPStan\Testing\assertType;

/** @SuppressWarnings("PHPMD.UnusedPrivateField") */
class GetNonPublicPropertyDynamicClass
{
    /** @var string[] */
    protected array $propertyArray = ['foo', 'Bar'];

    /** @var bool */
    protected bool $propertyBool = true;

    /** @var int */
    protected int $propertyInteger = 42;

    /** @var object */
    protected object $propertyObject;

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->propertyObject = $this;
    }
}

class GetNonPublicPropertyDynamicTest extends BaseTestCase
{
    public function testSuccess(): void
    {
        $someClass = new GetNonPublicPropertyDynamicClass();
        $array = $this->getNonPublicPropertyValue($someClass, 'propertyArray');
        $bool = $this->getNonPublicPropertyValue($someClass, 'propertyBool');
        $integer = $this->getNonPublicPropertyValue($someClass, 'propertyInteger');
        $object = $this->getNonPublicPropertyValue($someClass, 'propertyObject');
        if (function_exists('assertType') === true) {
            // PHPStan checks
            assertType('array<string>', $array);
            assertType('bool', $bool);
            assertType('int', $integer);
            assertType('object', $object);
        }
        self::assertCount(2, $array);
    }
}
