<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MultipleClasses
declare(strict_types=1);

namespace Interitty\PhpUnit;

class GetSetBoolClass
{
    /** @var bool */
    protected bool $foo = false;

    /**
     * Foo checker
     *
     * @return bool
     */
    protected function isFoo(): bool
    {
        return $this->foo;
    }

    /**
     * Foo setter
     *
     * @param bool $foo
     * @return static Provides fluent interface
     */
    protected function setFoo(bool $foo)
    {
        $this->foo = $foo;
        return $this;
    }
}

class GetSetBoolTest extends BaseTestCase
{
    public function testSuccess(): void
    {
        $this->processTestGetSetBool(GetSetBoolClass::class, 'foo', true);
    }
}
