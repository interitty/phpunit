<?php

declare(strict_types=1);

namespace Interitty\PhpUnit;

use Interitty\Exceptions\Exceptions;
use LogicException;
use Throwable;

class ExceptionCallbackTest extends BaseTestCase
{
    public function testExtendTranslate(): void
    {
        $this->expectExceptionCallback(static function (Throwable $exception): void {
            self::assertSame('Message with key "foo"', (string) $exception);
        });

        throw Exceptions::extend(LogicException::class)
                ->setMessage('Message with key ":key"')
                ->addData('key', 'foo');
    }
}
