<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MultipleClasses
declare(strict_types=1);

namespace Interitty\PhpUnit;

abstract class FooClass
{
    abstract public function abstractMethod(): bool;

    public function isAccessible(): bool
    {
        return true;
    }
}

class MockAbstractTest extends BaseTestCase
{
    public function testSuccess(): void
    {
        $class = $this->createMockAbstract(FooClass::class, ['abstractMethod']);
        $class->expects(self::once())->method('abstractMethod')->willReturn(true);
        self::assertTrue($class->isAccessible());
        self::assertTrue($class->abstractMethod());
    }
}
