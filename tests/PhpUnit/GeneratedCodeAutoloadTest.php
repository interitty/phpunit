<?php

declare(strict_types=1);

namespace Interitty\PhpUnit;

use Interitty\Utils\Strings;
use Nette\PhpGenerator\Helpers;

use function class_exists;

class GeneratedCodeAutoloadTest extends BaseTestCase
{
    /** All available class / interface / namespace name constants */
    protected const string NAME_NAMESPACE = 'Vendor\\Namespace';
    protected const string NAME_DUMMY_CLASS = self::NAME_NAMESPACE . '\\DummyClass';

    public function testSuccess(): void
    {
        /**
         * @phpstan-var class-string $className
         * @phpstan-ignore varTag.nativeType
         */
        $className = self::NAME_DUMMY_CLASS;

        self::assertFalse(class_exists($className));
        $this->generateDummyClass($className);
        self::assertTrue(class_exists($className));
        self::assertInstanceOf($className, new $className());
    }

    /**
     * Dummy class generator
     *
     * @param string $className
     * @return void
     */
    protected function generateDummyClass(string $className): void
    {
        $classShortName = Helpers::extractShortName($className);
        $namespace = Strings::before($className, '\\' . $classShortName, -1);
        $code = '<?php
declare(strict_types=1);

' . ((string) $namespace === '' ? '' : 'namespace ' . $namespace . ';') . '

class ' . $classShortName . ' {
}
';
        $this->processRegisterAutoload($className, $code);
    }
}
