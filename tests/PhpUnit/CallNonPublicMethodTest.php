<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MultipleClasses
declare(strict_types=1);

namespace Interitty\PhpUnit;

class CallNonPublicMethodClass
{
    protected function isAccessible(): bool
    {
        return true;
    }
}

class CallNonPublicMethodTest extends BaseTestCase
{
    public function testSuccess(): void
    {
        $class = new CallNonPublicMethodClass();
        $isAccessible = $this->callNonPublicMethod($class, 'isAccessible');
        self::assertTrue($isAccessible);
    }
}
