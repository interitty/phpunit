<?php

declare(strict_types=1);

namespace Interitty\PHPStan\Type;

use Interitty\PhpUnit\BaseTestCase;
use PhpParser\Node\Expr\MethodCall;
use PHPStan\Analyser\Scope;
use PHPStan\Reflection\MethodReflection;
use PHPStan\Reflection\ParametersAcceptorSelector;
use PHPStan\Type\DynamicMethodReturnTypeExtension;
use PHPStan\Type\Type;

class PropertyDynamicReturnTypeExtension implements DynamicMethodReturnTypeExtension
{
    /**
     * @inheritDoc
     */
    public function getClass(): string
    {
        return BaseTestCase::class;
    }

    /**
     * @inheritDoc
     */
    public function isMethodSupported(MethodReflection $methodReflection): bool
    {
        $methodName = $methodReflection->getName();
        $methodSupported = ($methodName === 'getNonPublicPropertyValue');
        return $methodSupported;
    }

    /**
     * @inheritDoc
     */
    public function getTypeFromMethodCall(MethodReflection $method, MethodCall $methodCall, Scope $scope): Type
    {
        $arguments = $methodCall->getArgs();
        $returnType = ParametersAcceptorSelector::selectFromArgs($scope, $arguments, $method->getVariants())
            ->getReturnType();
        $objectType = $scope->getType($arguments[0]->value);
        $propertyNameType = $scope->getType($arguments[1]->value);
        foreach ($propertyNameType->getConstantStrings() as $stringType) {
            $propertyName = $stringType->getValue();
        }
        if (isset($propertyName) && ($objectType->hasProperty($propertyName)->yes())) {
            $property = $objectType->getProperty($propertyName, $scope);
            $returnType = $property->getReadableType();
        }
        return $returnType;
    }
}
