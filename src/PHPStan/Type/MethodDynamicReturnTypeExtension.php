<?php

declare(strict_types=1);

namespace Interitty\PHPStan\Type;

use Interitty\PhpUnit\BaseTestCase;
use PhpParser\Node\Expr\MethodCall;
use PHPStan\Analyser\Scope;
use PHPStan\Reflection\MethodReflection;
use PHPStan\Reflection\ParametersAcceptorSelector;
use PHPStan\Type\DynamicMethodReturnTypeExtension;
use PHPStan\Type\Type;

class MethodDynamicReturnTypeExtension implements DynamicMethodReturnTypeExtension
{
    /**
     * @inheritDoc
     */
    public function getClass(): string
    {
        return BaseTestCase::class;
    }

    /**
     * @inheritDoc
     */
    public function isMethodSupported(MethodReflection $methodReflection): bool
    {
        $methodName = $methodReflection->getName();
        $methodSupported = ($methodName === 'callNonPublicMethod');
        return $methodSupported;
    }

    /**
     * @inheritDoc
     */
    public function getTypeFromMethodCall(MethodReflection $method, MethodCall $methodCall, Scope $scope): Type
    {
        $arguments = $methodCall->getArgs();
        $returnType = ParametersAcceptorSelector::selectFromArgs($scope, $arguments, $method->getVariants())
            ->getReturnType();
        $objectType = $scope->getType($arguments[0]->value);
        $methodNameType = $scope->getType($arguments[1]->value);
        foreach ($methodNameType->getConstantStrings() as $stringType) {
            $methodName = $stringType->getValue();
        }
        if (isset($methodName) && ($objectType->hasMethod($methodName)->yes())) {
            $method = $objectType->getMethod($methodName, $scope);
            $returnType = ParametersAcceptorSelector::selectFromArgs($scope, $arguments, $method->getVariants())
                ->getReturnType();
        }
        return $returnType;
    }
}
