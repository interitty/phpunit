<?php

declare(strict_types=1);

namespace Interitty\PhpUnit;

use Interitty\Exceptions\ExtendedExceptionPhpunitTrait;
use Interitty\Utils\FileSystem;
use Interitty\Utils\ReflectionObject;
use Interitty\Utils\Strings;
use Interitty\Utils\Validators;
use Nette\Utils\AssertionException;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use Traversable;

use function assert;
use function iterator_to_array;
use function spl_autoload_register;

use const DIRECTORY_SEPARATOR;

/**
 * @phpstan-type AssertionExceptionData array{
 *      'expected': string,
 *      'label'?: string,
 *      'type': string,
 *  }
 * @SuppressWarnings("PHPMD.NumberOfChildren")
 */
abstract class BaseTestCase extends TestCase
{
    use DataProvider;
    use ExtendedExceptionPhpunitTrait {
        setUp as extendedSetUp;
    }

    /** @var array<string, string> */
    protected static array $autoloader = [];

    /** @var string|null */
    protected string|null $vfsRoot;

    /**
     * @inheritdoc
     * @return void
     * @codeCoverageIgnore
     */
    public static function setUpBeforeClass(): void
    {
        static $autoloaderRegisterd = false;
        parent::setUpBeforeClass();

        if ($autoloaderRegisterd === false) {
            /** @phpstan-var callable(string): void $callback */
            $callback = 'self::processAutoload';
            $autoloaderRegisterd = spl_autoload_register($callback);
        }
    }

    /**
     * @inheritdoc
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->extendedSetUp();
        $this->setVfsRoot();
    }

    // <editor-fold defaultstate="collapsed" desc="Additional assertions">
    /**
     * Asserts that a two variables have the same iterable content.
     *
     * @param iterable<mixed> $expected
     * @param iterable<mixed> $actual
     * @param string $message [OPTIONAL]
     * @return void
     */
    public static function assertSameContent(iterable $expected, iterable $actual, string $message = ''): void
    {
        $expectedArray = $expected instanceof Traversable ? iterator_to_array($expected) : $expected;
        $actualArray = $actual instanceof Traversable ? iterator_to_array($actual) : $actual;
        static::assertSame($expectedArray, $actualArray, $message);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * Mock for abstract class factory
     *
     * @param string $className
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @param string[] $addMethods [OPTIONAL] List of mocked undeffined method names
     * @return MockObject
     * @template RealInstanceType of object
     * @phpstan-param class-string<RealInstanceType> $className
     * @phpstan-return RealInstanceType&MockObject
     */
    protected function createMockAbstract(string $className, array $methods = [], array $addMethods = []): MockObject
    {
        $mock = $this->getMockBuilder($className)
            ->disableOriginalConstructor()
            ->allowMockingUnknownTypes()
            ->onlyMethods($methods)
            ->addMethods($addMethods)
            ->getMockForAbstractClass();
        return $mock;
    }

    /**
     * Temp directory factory
     *
     * @param string $directoryName [OPTIONAL]
     * @return string Path of the directory
     */
    protected function createTempDirectory(string $directoryName = 'temp'): string
    {
        $vfsRoot = $this->getVfsRoot();
        $directoryPath = $vfsRoot . DIRECTORY_SEPARATOR . $directoryName;
        FileSystem::createDir($directoryPath);
        return $directoryPath;
    }

    /**
     * Temp file factory
     *
     * @param string $content [OPTIONAL]
     * @param string $fileName [OPTIONAL]
     * @return string Path of the file
     */
    protected function createTempFile(string $content = '', string $fileName = 'temp'): string
    {
        $vfsRoot = $this->getVfsRoot();
        $filePath = $vfsRoot . DIRECTORY_SEPARATOR . $fileName;
        FileSystem::write($filePath, $content);
        return $filePath;
    }

    /**
     * Vfs root url factory
     *
     * @param string $rootDirName [OPTIONAL]
     * @return string Path of the VfsRoot url
     * @internal
     */
    protected function createVfsRootUrl($rootDirName = 'root'): string
    {
        $vfsRoot = vfsStream::setup($rootDirName)->url();
        return $vfsRoot;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Allow call protected methods
     *
     * @param object $object
     * @param string $methodName
     * @param mixed[] $arguments [OPTIONAL] Arguments of the called method
     * @return mixed Depend on the called method
     */
    protected function callNonPublicMethod($object, string $methodName, array $arguments = []): mixed
    {
        $class = $this->createReflectionClass($object);
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);
        return $method->invokeArgs($object, $arguments);
    }

    /**
     * ReflectionClass factory
     *
     * @param string|object $object
     * @return ReflectionClass
     * @template RealInstanceType of object
     * @phpstan-param class-string<RealInstanceType>|RealInstanceType $object
     * @phpstan-return ReflectionClass<RealInstanceType>
     */
    protected function createReflectionClass($object): ReflectionClass
    {
        $class = new ReflectionClass($object);
        return $class;
    }

    /**
     * Autoloader processor
     *
     * @param string $className
     * @return void
     */
    protected static function processAutoload(string $className): void
    {
        $autoloader = self::$autoloader;
        $classFileName = Strings::replace($className, '~\\\\~', '');
        if (isset($autoloader[$classFileName]) === true) {
            require_once($autoloader[$classFileName]);
        }
    }

    /**
     * Register code to autoloader processor
     *
     * @param string $className
     * @param string $code
     * @return void
     */
    protected function processRegisterAutoload(string $className, string $code): void
    {
        $classFileName = Strings::replace($className, '~\\\\~', '');
        if (isset(self::$autoloader[$classFileName]) === false) {
            self::$autoloader[$classFileName] = $this->createTempFile($code, $classFileName . '.php');
        }
    }

    /**
     * Allow access private property content
     *
     * @param object $object
     * @param string $propertyName
     * @return mixed Depend on the property value
     */
    protected function getNonPublicPropertyValue($object, string $propertyName): mixed
    {
        return ReflectionObject::getNonPublicPropertyValue($object, $propertyName);
    }

    /**
     * Allow access private property content
     *
     * @param object $object
     * @param string $propertyName
     * @param mixed $value Depend on the property value
     * @return void
     */
    protected function setNonPublicPropertyValue($object, string $propertyName, $value): void
    {
        ReflectionObject::setNonPublicPropertyValue($object, $propertyName, $value);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Standard testers">
    /**
     * Tester of standard bool getter for default value
     *
     * @param MockObject|string $className
     * @param string $propertyName
     * @param bool $default
     * @return void
     * @template RealInstanceType of object
     * @phpstan-param MockObject|class-string<RealInstanceType> $className
     */
    protected function processTestGetBoolDefault($className, string $propertyName, bool $default): void
    {
        $classMock = ($className instanceof MockObject) ? $className : $this->createPartialMock($className, []);
        $property = Strings::firstUpper($propertyName);
        static::assertSame($default, $this->callNonPublicMethod($classMock, 'is' . $property));
    }

    /**
     * Tester of standard getter for default value
     *
     * @param MockObject|string $className
     * @param string $propertyName
     * @param mixed $default
     * @return void
     * @template RealInstanceType of object
     * @phpstan-param MockObject|class-string<RealInstanceType> $className
     */
    protected function processTestGetDefault($className, string $propertyName, $default): void
    {
        $classMock = ($className instanceof MockObject) ? $className : $this->createPartialMock($className, []);
        $property = Strings::firstUpper($propertyName);
        static::assertSame($default, $this->callNonPublicMethod($classMock, 'get' . $property));
    }

    /**
     * Tester of standard getter/setter implementation
     *
     * @param MockObject|string $className
     * @param string $propertyName
     * @param mixed $value
     * @return void
     * @template RealInstanceType of object
     * @phpstan-param MockObject|class-string<RealInstanceType> $className
     */
    protected function processTestGetSet($className, string $propertyName, $value): void
    {
        $classMock = ($className instanceof MockObject) ? $className : $this->createPartialMock($className, []);
        $property = Strings::firstUpper($propertyName);
        static::assertSame($classMock, $this->callNonPublicMethod($classMock, 'set' . $property, [$value]));
        static::assertSame($value, $this->callNonPublicMethod($classMock, 'get' . $property));
    }

    /**
     * Tester of standard bool getter/setter implementation
     *
     * @param MockObject|string $className
     * @param string $propertyName
     * @param bool $value
     * @return void
     * @template RealInstanceType of object
     * @phpstan-param MockObject|class-string<RealInstanceType> $className
     */
    protected function processTestGetSetBool($className, string $propertyName, bool $value): void
    {
        $classMock = ($className instanceof MockObject) ? $className : $this->createPartialMock($className, []);
        $property = Strings::firstUpper($propertyName);
        static::assertSame($classMock, $this->callNonPublicMethod($classMock, 'set' . $property, [$value]));
        static::assertSame($value, $this->callNonPublicMethod($classMock, 'is' . $property));
    }

    /**
     * Tester of standard getter for missing mandatory value
     *
     * @param MockObject|string $className
     * @param string $propertyName
     * @return void
     * @template RealInstanceType of object
     * @phpstan-param MockObject|class-string<RealInstanceType> $className
     */
    protected function processTestGetUndefined($className, string $propertyName): void
    {
        $classMock = ($className instanceof MockObject) ? $className : $this->createPartialMock($className, []);
        $property = Strings::firstUpper($propertyName);
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('The :label expects to be :expected');
        $this->expectExceptionData([
            'expected' => 'initialized',
            'label' => $propertyName . ' before get',
        ]);
        $this->callNonPublicMethod($classMock, 'get' . $property);
    }

    /**
     * Tester of standard setter for overwriting already defined value
     *
     * @phpstan-param MockObject|class-string<RealInstanceType> $className
     * @param string $propertyName
     * @param mixed $value
     * @return void
     * @template RealInstanceType of object
     */
    protected function processTestSetAlreadyDefined($className, string $propertyName, $value): void
    {
        $classMock = ($className instanceof MockObject) ? $className : $this->createPartialMock($className, []);
        $property = Strings::firstUpper($propertyName);
        $this->callNonPublicMethod($classMock, 'set' . $property, [$value]);

        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('The :label expects to be :expected');
        $this->expectExceptionData([
            'expected' => 'uninitialized',
            'label' => $propertyName . ' before set',
        ]);
        $this->callNonPublicMethod($classMock, 'set' . $property, [$value]);
    }

    /**
     * Tester of standard setter for inserting unsupported value
     *
     * @param MockObject|string $className
     * @param string $propertyName
     * @param mixed $value
     * @phpstan-param AssertionExceptionData $data
     * @return void
     * @template RealInstanceType of object
     * @phpstan-param MockObject|class-string<RealInstanceType> $className
     */
    protected function processTestSetUnsupportedValue($className, string $propertyName, $value, array $data): void
    {
        $classMock = ($className instanceof MockObject) ? $className : $this->createPartialMock($className, []);
        $property = Strings::firstUpper($propertyName);
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('The :label expects to be :expected, :type given');
        $this->expectExceptionData([
            'expected' => $data['expected'],
            'label' => $data['label'] ?? $property,
            'type' => $data['type'],
        ]);
        $this->callNonPublicMethod($classMock, 'set' . $property, [$value]);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * VfsRoot getter
     *
     * @param bool $need [OPTIONAL] Create default one if not set before?
     * @return string|null
     * @internal
     */
    protected function getVfsRoot(bool $need = true): ?string
    {
        if (($this->vfsRoot === null) && ($need === true)) {
            $vfsRoot = $this->createVfsRootUrl();
            $this->setVfsRoot($vfsRoot);
        }
        return $this->vfsRoot;
    }

    /**
     * VfsRoot setter
     *
     * @param string|null $vfsRoot
     * @return static Provides fluent interface
     * @internal
     */
    protected function setVfsRoot(string $vfsRoot = null): static
    {
        assert(Validators::check($vfsRoot, 'uri|null', 'vfsRoot'));
        $this->vfsRoot = $vfsRoot;
        self::$autoloader = [];
        return $this;
    }

    // </editor-fold>
}
