<?php

declare(strict_types=1);

namespace Interitty\PhpUnit;

use Dibi\Connection;
use Dibi\Exception as DibiException;
use Interitty\Exceptions\Exceptions;
use LogicException;

/** @SuppressWarnings("PHPMD.NumberOfChildren") */
abstract class BaseDibiTestCase extends BaseTestCase
{
    /** @var mixed[] */
    protected array $config = [
        'driver' => 'pdo',
        'dsn' => 'sqlite::memory:',
    ];

    /** @var Connection */
    protected Connection $connection;

    /**
     * Setup database structure and content
     *
     * @param Connection $connection
     * @return void
     * @throws DibiException
     */
    protected function setupDatabase(Connection $connection): void
    {
        self::assertTrue($connection->isConnected());
    }

    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Connection factory
     *
     * @return Connection
     */
    protected function createConnection(): Connection
    {
        try {
            $config = $this->getConfig();
            $connection = new Connection($config);
            $this->setupDatabase($connection);
            return $connection;
        } catch (DibiException $exception) {
            throw Exceptions::extend(LogicException::class)
                ->setMessage($exception->getMessage())
                ->setCode($exception->getCode())
                ->setPrevious($exception);
        }
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Config getter
     *
     * @return mixed[]
     */
    protected function getConfig(): array
    {
        return $this->config;
    }

    /**
     * Config setter
     *
     * @param mixed[] $config
     * @return static Provides fluent interface
     */
    protected function setConfig(array $config): static
    {
        $this->config = $config;
        return $this;
    }

    /**
     * Connection getter
     *
     * @return Connection
     */
    protected function getConnection(): Connection
    {
        if (isset($this->connection) === false) {
            $connection = $this->createConnection();
            $this->setConnection($connection);
        }
        return $this->connection;
    }

    /**
     * Connection setter
     *
     * @param Connection $connection
     * @return static Provides fluent interface
     */
    protected function setConnection(Connection $connection): static
    {
        $this->connection = $connection;
        return $this;
    }

    // </editor-fold>
}
