<?php

declare(strict_types=1);

namespace Interitty\PhpUnit;

use Generator;

trait DataProvider
{
    /**
     * Standard set of values for testing string supported values
     *
     * @return Generator<string, array<string>>
     */
    public static function stringDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="[a-z] string">
        yield '[a-z] string' => ['string'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="[a-Z] string">
        yield '[a-Z] string' => ['String'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="[a-Z0-9] string">
        yield '[a-Z0-9] string' => ['String1234'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="UTF-8 string">
        yield 'UTF-8 string' => ['ěščřžýáíéůúňťĚŠČŘŽÝÁÍÉŮÚŇŤ'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Special characters string">
        yield 'Special characters string' => ['~*!-.:/\\"\''];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Emoji string">
        yield 'Emoji string' => ['😎'];
        // </editor-fold>
    }

    /**
     * Standard set of values for testing string unsupported values
     *
     * @return Generator<string, array{0: mixed, 1: array{'expected': string, 'type': string}}>
     */
    public static function unsupportedStringDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="array">
        yield 'array' => [[], ['expected' => 'string', 'type' => 'array']];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="object">
        yield 'object' => [(object) [], ['expected' => 'string', 'type' => 'object stdClass']];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="bool">
        yield 'bool' => [false, ['expected' => 'string', 'type' => 'bool false']];
        // </editor-fold>
    }
}
