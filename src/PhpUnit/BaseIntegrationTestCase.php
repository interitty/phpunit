<?php

declare(strict_types=1);

namespace Interitty\PhpUnit;

use Nette\Bootstrap\Configurator;
use Nette\DI\Container;

abstract class BaseIntegrationTestCase extends BaseTestCase
{
    /**
     * DI configurator factory
     *
     * <code>testName</code> parameter is for creating new container each test
     * @param string $configFilePath
     * @return Configurator
     */
    protected function createConfigurator(string $configFilePath): Configurator
    {
        $resetConfigContent = '
application:
    scanDirs: false

services:
    cacheStorage:
        class: Nette\Caching\Storages\MemoryStorage
';

        $resetConfigFilePath = $this->createTempFile($resetConfigContent, 'nette-reset.neon');
        $tempDir = $this->createTempDirectory();
        $configurator = new Configurator();
        $configurator->addStaticParameters(['testName' => $this->getName()]);
        $configurator->setTempDirectory($tempDir);
        $configurator->addConfig($resetConfigFilePath);
        $configurator->addConfig($configFilePath);
        return $configurator;
    }

    /**
     * DI container factory
     *
     * @param string $configFilePath
     * @return Container
     */
    protected function createContainer(string $configFilePath): Container
    {
        $configurator = $this->createConfigurator($configFilePath);
        $container = $configurator->createContainer();
        return $container;
    }
}
